import random
n = random.randint(4,30)
count_move = 0

while n > 1:
    print(f"Камней в куче: {n}\n")
    if count_move % 2 == 0:
        print("Ваш ход, игрок!")
        print("Возьмите из кучи 1, 2 или 3 камня\n")
        player_move = int(input())
        while player_move not in range(1,4):
            print("Введено недопустимое значение.\nВведите числа 1, 2 или 3\n")
            player_move = int(input())
        print(f"Игрок взял из кучи {player_move} камней\n")
        n = n - player_move
        if n == 1:
            print(f"Камней в куче: {n}\n")
            print("ПОЗДРАВЛЯЮ С ПОБЕДОЙ, ИГРОК")
    if count_move % 2:
        print("Ход делает робот\n")
        if n - 1 <= 3:
            robot_move = n - 1;
            print(f"Роботом взято из кучи камней")
            n = n - robot_move
            print(f"Камней в куче: {n}\n")
            print("ПОБЕДИЛ РОБОТ")
        else:
            robot_move = random.randint(1,3)
            print(f"Робот взял из кучи {robot_move} камней\n")
            n = n - robot_move
    count_move += 1